var limit = 3; // Number of items to display
var count = 0;

$(document).ready(function() {

    var feed = "http://walmartconnectedlife.wordpress.com/feed/";
    var stage = $("#widget");
    var query = "select * from feed where url='" + feed + "' LIMIT " + limit;
    var url = "http://query.yahooapis.com/v1/public/yql?q=" + encodeURIComponent(query) + "&format=json&callback=?";

    $.getJSON(url,function(data) {
        stage.empty();

        $.each(data.query.results.item || data.query.results.entry, function() {
            try {
                count++;
                stage.append(rss(this));
            }
            catch(e) {
                console.log("Error occurred when appending widget" + e);
            }
        })
    })

});

function rss(item) {

    // Prevent divider on last item
    var divider = '';
    if (count != limit) {
        divider = '<div class="divider"></div>';
    } 

    // Replace HTML of div element with the following
    return $('<div>').html(
        '<div class="itemTitle">' + '<a href="' + item.link + '">' + truncate_title(item.title) + '</a></div>'
        + '<div class="itemDate">' + item.creator + " - " + formatDate(item.pubDate) + '</div>'
        + '<div class="itemContent">' + truncate_content(item.description) + '</div>'
        + divider
    );
}

function truncate_title(title) {
    if (title.length > 60) {
        return title.substring(0,56) + '...';
    }
    return title;
}

function truncate_content(content) {
    if(content.length > 80) {
        return content.substring(0,76) + "...";
    }
    return content;
}

function formatDate(date) {
    return date.match(/\d+\s\w+\s\d+/)[0];
}